
// for loop
const cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];

let text = ""; 

for(i=0; i< cars.length; i++) {
    text+=cars[i]+" ";
}
console.log(text);
// concat birleştirme
const friends =["furkan","Mehmet","Ahmet"];
const bff=["Taha","Mehmet","Demir"];

const allFriends = friends.concat(bff);

console.log(allFriends);
// Sıralama
const points = [40, 100, 1, 5, 25, 10];
points.sort(function(a, b){return a - b});

//forEach 
const numbers = [45, 4, 9, 16, 25];
let txt=""; 
myFunction= value=> {
    txt+=value+" ";
}
numbers.forEach(myFunction);   
console.log(txt);

//map her biri için aynı işlem yapılmak istendiğinde
const numbers1 =[1,23,4,5,6,7];
console.log("Numbers1 arrayi: "+numbers1)
const numbers2=numbers1.map(multiplyArray);
   function multiplyArray(value){
        return value*2;
   }
console.log("Numbers2 arrayi: "+numbers2);

//filter 
const cards = [1,2,3,4,5,6,7,8,9];
const over5= cards.filter(cardsNumber);
function cardsNumber(value){
    return value>5;
}
console.log("Besten Büyük sayılar: "+over5);

//indexOf 

const fruits = ["apple","banana","strawberry","berry"]; 
const appleLoc= fruits.indexOf("apple")+1;
console.log("Apple location is: "+appleLoc);

// Create a Map ???? 
const fruits2 = new Map([
    ["apples", 500],
    ["bananas", 300],
    ["oranges", 200]
  ]);
  
  let text2 = "";
  for (const x of fruits.entries()) {
    text2 += x + "burada";
  }
console.log(fruits2);

// replace
  let text4="Aşk karın doyurur";
  console.log(text4);

  text4=text4.replace("Aşk","Para");
  console.log(text4);

  let text5 = "Visit my site!!";
  console.log(text5)
  let mentText=text5.replace(/site/i,"Menthouse")
  console.log(mentText);

  // this obje içinde function kullanırken user yerine this.

  const user = {
    id:"Furkan",
    lastName:"Annen",
    vallet: 40,
    fullName: function(){
        return this.id+" "+this.lastName;
        
    }
  }
  console.log(user.fullName());

  // call obje içindeki function'u farklı objede çalıştırmak için çağırıyor.
  const personCaller ={
    fullName2(){
        return this.id+ " " + this.lastName
    }
  }
  const person2= {
    id:"falafel",
    lastName:"demir",
  }
  let m = personCaller.fullName2.call(person2);
  console.log(m);

  //switch case 
  switch (new Date().getDay()) {
    case 0:
      day = "Sunday";
      break;
    case 1:
      day = "Monday";
      break;
    case 2:
      day = "Tuesday";
      break;
    case 3:
      day = "Wednesday";
      break;
    case 4:
      day = "Thursday";
      break;
    case 5:
      day = "Friday";
      break;
    case 6:
      day = "Saturday";
      break;
    default:
      day = "Unknown";
  }

  // for loop example 
  const person3 = {
    fname:"Furkan",
    lname:"şimşek",
    age: 24,
  }
  let txt5="";
    for (let x in person3){
        txt5+=person3[x]+" ";
    }
  console.log(txt5);
    
  
  //forEach-map practice 
  const large={size:"large",stock:50,price:50};
  const medium={size:"medium",stock:40,price:60};
  const small={size:"small",stock:30,price:70};
  
  const sizes=[large,medium,small];
      const initialize =()=>{
        sizes.forEach((x)=>{
          console.log(x.stock+" tane "+x.size);
        });
      }
      initialize();
      //example1
      const filterData= sizes.filter((ele)=>ele.stock<=40);
      console.log(filterData);
      //example2
      const newData2= sizes.map((el)=>{
        return {
          ...el,
            brand:"Zara"
        }
      })
      console.log(newData2);
      
      //prime number flag örneği;

for (var counter = 0; counter <= 1000; counter++) {
  //21 de flag
  var notPrime = false;
  for (var i = 2; i <= counter; i++) {
       if (counter%i===0 && i!==counter) {
      //25 te flag
          notPrime = true;
      }
  }
  if (notPrime === false) {
              console.log(counter);
  }
}
//İki sayının çarpımı fonksiyonu
function carpma(a,b) {
  var sonuc=a*b;
  console.log(sonuc);
}

carpma(2,5);

//Kaç kere tekrar isteniyorsa o kadar I love Menthouse yazıyor
var kacKere=20;
  biSeyYap=()=>
      console.log("I love Menthouse");
for (var i=0; i<kacKere; i++){
  biSeyYap();
}



    /*  const stockArray=[];
      const newArray=sizes.forEach((x)=>{
        stockArray.push(x.size+": "+x.stock)});
        console.log(stockArray);*/
        
   /*   for (var i=0;i<sizes.length;i++){
            let flag =false;
            if(i<=sizes.length){
                flag=true;
            sizeValue=sizes[i].size+" "+sizes[i].stock;}
            else {
                flag=false;
                break;
            }
        if(flag=true){
            console.log(sizeValue);
        }
    }*/
        
    



/* Use "" instead of new String()
Use 0 instead of new Number()
Use false instead of new Boolean()
Use {} instead of new Object()
Use [] instead of new Array()
Use /()/ instead of new RegExp()
Use function (){} instead of new Function()*/
